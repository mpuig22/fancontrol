# 1. Introduction
This a python script to control de power fan of a Raspberry PI.

# 2. Details
## For further details of background See my [wiki](https://gitlab.com/elmolina/raspberrypi/fancontrol/wikis/home) witch contains details abouy the circuit design.

# 3. Software
- Install IOTstack. IOTstack is a builder for docker-compose to easily make and maintain IoT stacks on the Raspberry Pi
  - ``git clone https://github.com/gcgarner/IOTstack.git``
  - Execure ``menu.sh``localed in the IOTstack directory
  - Install ``Docker``
  - In ``Build Stack``menu, select ``nodered``, ``mosquitto``, ``influxdb`` and ``grafana``
  - Exeute mosquito script to solve directory permission problems located in: /IOTstack/services/mosquitto/directoryfix.sh
  - In ``Miscellaneous command`` menu, select ``Disable swap by setting swappiness to 0`` and ``install log2ram to decrease load on sd card, moves /var/log into ram``

- Install MQTT: https://github.com/eclipse/paho.mqtt.python
  - ``sudo apt install paho-mqtt``

- Make fancontrol.sh executable on boot:
  - Configure fancontrol.ini
  - Copy fancontrol.sh file to /etc/init.d, and make it executable:
    ```
    sudo cp fancontrol.sh /etc/init.d/
    sudo chmod +x /etc/init.d/fancontrol.sh
    sudo touch /var/log/fancontrol.log && sudo chown elmol /var/log/fancontrol.log
    ```
  - Now we'll register the script to be run on boot:
    ```
    sudo update-rc.d fancontrol.sh defaults
    ```
  - Now, you can either restart your machine, or kick this off manually since it won't already be running:
    ```
    sudo reboot
    ```
    or
    ```
    sudo /etc/init.d/fancontrol.sh start
    ```
    
- Configure nodered fluxex:
  - Remember using the docker base IP 172.18.0.1 to point to Mosquitto and InfluxDB ports

- Create InfluxDB database:
  - Enter to InfluxBD container
  ```
  docker exec -it influxdb bash
  ```
  - Execute
  ```
  influx
  CREATE DATABASE RASPBERRY
  use RASPBERRY
  show databases
  show MEASUREMENTS
  show FIELD KEYS
  select * from fan limit 10
  CREATE RETENTION POLICY "1_week_1_day_shard" ON "RASPBERRY" DURATION 1w REPLICATION 1 SHARD DURATION 1d DEFAULT
  DROP RETENTION POLICY "autogen" ON "RASPBERRY"
  ```

- Stress CPU: You can test de script by stressing the CPU with:
  ```
  sysbench --test=cpu --cpu-max-prime=20000 --num-threads=4 run
  ```
  
- Configure backup: https://rclone.org/drive/