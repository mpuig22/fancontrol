#!/usr/bin/env python3

import os
import PID
import RPi.GPIO as GPIO
import psutil
import configparser
import paho.mqtt.client as mqtt                        # see documentation in https://github.com/eclipse/paho.mqtt.python
import json
import math
import time

config_ini = "fancontrol.ini"
config_parser = configparser.ConfigParser()
config_parser.read(config_ini)                         # read config file and store its values in config_parser variable 

sampletime = config_parser.getint("PID", "sampletime")
min_fan_power = config_parser.getfloat("Fan", "min_fan_power")

mqtt_topic = config_parser.get("MQTT", "topic")

def initMQTT():
    mqttClient = mqtt.Client()
    try:         
        # Starts a new thread, that calls the loop method at regular intervals for you. It also handles re-connects automatically.
        mqttClient.loop_start()                        # Non blocking method. We want to manage the fan though no MQTT server is present
        mqtt_hostname = config_parser.get("MQTT", "host")    
        mqttClient.connect(mqtt_hostname)
        return mqttClient
    except:                                            # Error control. The program will continue though there is no MQTT server
        return mqttClient

def initGPIO():
    pin = config_parser.getint("Fan", "pin")
    frequency = config_parser.getint("Fan", "frequency")
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.OUT)
    PulseWidht=GPIO.PWM(pin, frequency)
    PulseWidht.start(0)
    return PulseWidht

def initPID():
    P = config_parser.getfloat("PID", "proporcional")
    I = config_parser.getfloat("PID", "integral")
    D = config_parser.getfloat("PID", "derivative")
    target_temperature = config_parser.getint("Fan", "target_temperature")
    windup = config_parser.getint("PID", "windup")

    pid = PID.PID(P, I, D)
    pid.setTarget(target_temperature)
    pid.setSampleTime(sampletime)
    pid.setWindup(windup)
    return pid

def getCPUtemperature():
    res = os.popen('vcgencmd measure_temp').readline()
    temp =(res.replace("temp=","").replace("'C\n",""))
    return float(temp)

def getCPUtemperatureTesting():
    temp = getCPUtemperature()
    return float(temp) - pid.output/5
  
def fanOFF(PulseWidht):
    PulseWidht.ChangeDutyCycle(0) # switch fan off
    return()

def setPin(mode): # A little redundant function but useful if you want to add logging
    GPIO.output(FANPIN, mode)
    return()

def round_floats(o): # Transform float numbers in json payload with 2 decimal places
    if isinstance(o, float): return round(o, 2)
    if isinstance(o, dict): return {k: round_floats(v) for k, v in o.items()}
    if isinstance(o, (list, tuple)): return [round_floats(x) for x in o]
    return o

def report(currentTemp, newPulseWidth):
    payload = {}
    
    payload['time'] = time.time_ns()                   # InfluxDB use nanoseconds for time field: https://discourse.nodered.org/t/time-stamp-and-influx-db-problem/13715
    payload['currentTemp'] = math.trunc(currentTemp)
    payload['fanSpeed'] = newPulseWidth            # Send the fan power as percentage (0-100)
    payload['cpu_load'] = psutil.cpu_percent()
    payload['pid_output'] = math.trunc(pid.output)
    payload['PTerm'] = pid.Kp * pid.PTerm
    payload['ITerm'] = pid.Ki * pid.ITerm
    payload['DTerm'] = pid.Kd * pid.DTerm
    json_data = json.dumps(round_floats(payload))   
    #print(json_data)
    return json_data    

def handleFan(pid, currentTemp):
    output = pid.update(currentTemp)                   # Performs new output based on current temperature
    output = min(pid.windup_guard, max(output, -pid.windup_guard)) # Assures that output is < windup and > -windup. These are the top an botton stardard values for output.
    fanSpeed = (output + pid.windup_guard) / (pid.windup_guard*2) # transform output in a power percentage

    if fanSpeed <= min_fan_power:                      # Control minimal power fan
      fanSpeed = 0
    fanSpeed = min( 1, fanSpeed)                       # Control maximun power fan

    return(fanSpeed)

try:
    PulseWidht = initGPIO()                            # init GPIO with config parameters
    pid = initPID()                                    # init PID with config parameters
    mqttClient = initMQTT()                            # init MQTT client with config parameters
    print ("Loading configuration OK")
    while True:
        currentTemp = getCPUtemperature()              # Reads current CPU temperature
        newPulseWidth = handleFan(pid, currentTemp)*100    # Returns new power fan to adjust temperature

        json_data = report(currentTemp, newPulseWidth) # Create json with data
        mqttClient.publish(mqtt_topic, json_data)      # Report data to MQTT server
        
        PulseWidht.ChangeDutyCycle(newPulseWidth)      # Change power of the fan to adjust to target temperature
        
        time.sleep(sampletime)                         # Read the temperature every sample time sec, increase or decrease this limit if you want 
except KeyboardInterrupt:                              # trap a CTRL+C keyboard interrupt 
    fanOFF(PulseWidht)                                 # Shutdown fan 
    GPIO.cleanup()                                     # resets all GPIO ports used by this program
